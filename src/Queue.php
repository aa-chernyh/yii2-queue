<?php

namespace queue;

use queue\models\IJob;
use queue\models\QueueTask;
use queue\exceptions\LogicException;
use Throwable;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\base\Component;
use yii\helpers\Inflector;
use yii\console\Application as ConsoleApp;

class Queue extends Component implements BootstrapInterface
{

    private const DEFAULT_ATTEMPTS_COUNT = 1;
    private const DEFAULT_MAX_SECONDS_TO_COMPLETE_TASK = 10800; // 3 часа

    /**
     * @var string
     */
    public $pathToYii;

    /**
     * @var integer
     */
    public $attempts;

    /**
     * @var integer
     */
    public $maxSecondsToCompleteTask;

    /**
     * Queue constructor.
     * @param array $config
     * @throws LogicException
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        if(!$this->pathToYii) {
            throw new LogicException('В конфигурации не указан параметр pathToYii');
        }
        if(!$this->attempts) {
            $this->attempts = self::DEFAULT_ATTEMPTS_COUNT;
        }
        if(!$this->maxSecondsToCompleteTask) {
            $this->maxSecondsToCompleteTask = self::DEFAULT_MAX_SECONDS_TO_COMPLETE_TASK;
        }
    }

    /**
     * @param Application $app
     * @throws LogicException
     */
    public function bootstrap($app)
    {
        if ($app instanceof ConsoleApp) {
            $app->controllerMap[$this->getCommandId()] = [
                'class' => QueueController::class,
                'pathToYii' => $this->pathToYii,
                'attempts' => $this->attempts,
                'maxSecondsToCompleteTask' => $this->maxSecondsToCompleteTask,
            ];
        }
    }

    /**
     * @return string
     * @throws LogicException
     */
    protected function getCommandId(): string
    {
        foreach (Yii::$app->getComponents(false) as $id => $component) {
            if ($component === $this) {
                return Inflector::camel2id($id);
            }
        }
        throw new LogicException('Queue must be an application component');
    }

    /**
     * @param IJob $job
     * @param int $priority 0...999
     * @throws Throwable
     */
    public static function push(IJob $job, int $priority = QueueTask::PRIORITY_DEFAULT): void
    {
        $task = new QueueTask();
        $task->status = QueueTask::STATUS_WAIT;
        $task->class_name = get_class($job);
        $task->params = json_encode(get_object_vars($job));
        $task->attempt = 1;
        $task->priority = $priority;

        $task->insert();
    }

    public static function cleanOldTasksByDate(string $date): void
    {
        $oldTaskIds = QueueTask::find()
            ->whereEndDateLessWhen($date)
            ->onlyId()
            ->column();
        QueueTask::deleteAll(['id' => $oldTaskIds]);
    }
}