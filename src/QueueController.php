<?php

namespace queue;

use \Throwable;
use yii\base\InvalidConfigException;
use yii\console\Controller;
use yii\db\StaleObjectException;

class QueueController extends Controller
{

    /**
     * @var QueueManager
     */
    private $queueManager;

    /**
     * @var string
     */
    public $pathToYii;

    /**
     * @var integer
     */
    public $attempts;

    /**
     * @var integer
     */
    public $maxSecondsToCompleteTask;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->queueManager = new QueueManager($this->pathToYii, $this->attempts, $this->maxSecondsToCompleteTask);
    }

    /**
     * @param int $taskId
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionRunJob(int $taskId): void
    {
        $this->queueManager->runJob($taskId);
    }

    /**
     * @throws InvalidConfigException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionRun(): void
    {
        $this->queueManager->run();
        $this->queueManager->holdProcessWhileJobsIsRunning();
    }

    /**
     * @throws InvalidConfigException
     */
    public function actionListen(): void
    {
        $this->queueManager->listen();
    }

    public function actionClear(): void
    {
        $this->queueManager->clear();
    }
}