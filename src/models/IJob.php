<?php

namespace queue\models;

interface IJob
{

    public function run(): void;
}