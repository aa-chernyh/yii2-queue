<?php

namespace queue\models;

use yii\base\BaseObject;

class JobExample extends BaseObject implements IJob
{

    public $param1;

    public function run(): void
    {
        file_put_contents('test.txt', $this->param1);
    }
}