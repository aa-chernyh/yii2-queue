<?php

namespace queue\models\activeQuery;

use yii\db\ActiveQuery;
use yii\db\Expression;

class QueueTaskActiveQuery extends ActiveQuery
{

    /**
     * @param string|string[] $status
     * @return $this
     */
    public function whereStatusIs($status): self
    {
        return $this->andWhere(['status' => $status]);
    }

    public function whereRunTimeMoreThen(int $seconds): self
    {
        return $this->andWhere(new Expression('TIMESTAMPDIFF(SECOND, start_date, NOW()) > ' . $seconds));
    }

    public function onlyId(): self
    {
        return $this->select(['id']);
    }

    public function whereEndDateLessWhen(string $date): self
    {
        return $this->andWhere(['<', 'end_date', $date]);
    }

    public function orderByPriority(): self
    {
        return $this->orderBy(['priority' => SORT_DESC]);
    }
}