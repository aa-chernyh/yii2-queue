<?php

namespace queue\models;

use queue\exceptions\LogicException;
use queue\helpers\Utils;
use queue\models\activeQuery\QueueTaskActiveQuery;
use DateTime;
use Throwable;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;

/**
 * @property integer $id
 * @property string $class_name Имя класса Job-а
 * @property string $params Json параметров класса Job-а
 * @property string $start_date Дата начала
 * @property string $end_date Дата окончания
 * @property integer $status Статус процесса
 * @property integer $pid Id процесса
 * @property string $error Текст ошибки
 * @property integer $attempt Номер попытки
 * @property integer $priority Приоритет
 * @property string $stacktrace StackTrace
 */
class QueueTask extends ActiveRecord
{

    const STATUS_WAIT = 'wait';
    const STATUS_RUN = 'run';
    const STATUS_FINISH = 'finish';
    const STATUS_ERROR = 'error';

    const PRIORITY_PARAM_NAME = 'priority';
    const PRIORITY_MAX = 999;
    const PRIORITY_DEFAULT = 100;
    const PRIORITY_MIN = 0;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['id', 'pid', 'attempt', self::PRIORITY_PARAM_NAME], 'integer'],
            [['start_date', 'end_date'], 'datetime', 'format' => 'php:' . Utils::DATETIME_FORMAT],
            [['class_name'], 'string', 'max' => 255],
            [['params'], 'string'],
            [['status'], 'string', 'max' => 63],
            [['error'], 'string', 'max' => 1023],
            [['class_name', 'status'], 'required'],
            [[self::PRIORITY_PARAM_NAME], 'validatePriority'],
        ];
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'queue_task';
    }

    public function validatePriority(): void
    {
        if(isset($this->priority)) {
            if($this->priority < self::PRIORITY_MIN) {
                $this->addError(self::PRIORITY_PARAM_NAME, 'Приоритет меньше минимального значения');
            }

            if($this->priority > self::PRIORITY_MAX) {
                $this->addError(self::PRIORITY_PARAM_NAME, 'Приоритет больше максимального значения');
            }
        }
    }

    /**
     * @param string $status
     * @return self[]
     * @throws InvalidConfigException
     */
    public static function getAllInStatus(string $status): array
    {
        return self::find()->where(['status' => $status])->all();
    }

    /**
     * @return QueueTaskActiveQuery
     * @throws InvalidConfigException
     */
    public static function find(): QueueTaskActiveQuery
    {
        return Yii::createObject(QueueTaskActiveQuery::class, [get_called_class()]);
    }

    /**
     * @param int $id
     * @return $this
     * @throws LogicException
     */
    public static function getById(int $id): self
    {
        $self = self::findOne(['id' => $id]);
        if(!$self) {
            throw new LogicException('Task с id ' . $id . ' не найден в БД');
        }

        return $self;
    }

    /**
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function setIsRunning(): void
    {
        $this->status = QueueTask::STATUS_RUN;
        $this->start_date = Utils::getDbDateTimeStringByDateTime(new DateTime());
        $this->update();
    }

    /**
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function setIsFinished(): void
    {
        $this->status = QueueTask::STATUS_FINISH;
        $this->end_date = Utils::getDbDateTimeStringByDateTime(new DateTime());
        $this->update();
    }

    /**
     * @param Throwable $exception
     * @param int $attempts
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function setError(Throwable $exception, int $attempts): void
    {
        $this->error = $exception->getMessage();
        $this->status = self::STATUS_ERROR;
        $this->end_date = Utils::getDbDateTimeStringByDateTime(new DateTime());
        $this->stacktrace = $exception->getTraceAsString();
        $this->update();

        if($this->attempt < $attempts) {
            $this->insertTaskToRetry();
        }
    }

    /**
     * @throws Throwable
     */
    private function insertTaskToRetry(): void
    {
        $newTask = new self();
        $newTask->class_name = $this->class_name;
        $newTask->params = $this->params;
        $newTask->status = self::STATUS_WAIT;
        $newTask->attempt = $this->attempt + 1;
        $newTask->insert();
    }
}