<?php

namespace queue;

use queue\exceptions\ConsoleException;
use queue\exceptions\LogicException;
use queue\helpers\ConsoleHelper;
use queue\helpers\Utils;
use queue\models\IJob;
use queue\models\QueueTask;
use ReflectionClass;
use Throwable;
use yii\base\InvalidConfigException;
use yii\db\StaleObjectException;

class QueueManager
{

    private const SLEEP_TIME_IN_SECONDS_IF_QUEUE_IS_EMPTY = 5;
    private const SLEEP_TIME_IN_SECONDS_IF_THREADS_IS_BUSY = 2;
    private const SLEEP_TIME_ON_HOLD_PROCESS = 2;

    private const MIN_COUNT_OF_AVAILABLE_THREADS = 1;

    private const DEAD_JOB_EXCEPTION_MESSAGE = 'Job завис и был исключен из очереди';

    private const RUN_JOB_CONSOLE_COMMAND_TEMPLATE = "php %s queue/run-job %s";

    /**
     * @var int
     */
    private $maxThreadsAvailable;

    /**
     * @var string
     */
    private $pathToYii;

    /**
     * @var integer
     */
    public $attempts;

    /**
     * @var integer
     */
    public $maxSecondsToCompleteTask;

    public function __construct(string $pathToYii, int $attempts, int $maxSecondsToCompleteTask)
    {
        $this->pathToYii = $pathToYii;
        $this->attempts = $attempts;
        $this->maxSecondsToCompleteTask = $maxSecondsToCompleteTask;

        try {
            $this->maxThreadsAvailable = ConsoleHelper::getCountOfThreads();
        } catch (ConsoleException $exception) {
            $this->maxThreadsAvailable = self::MIN_COUNT_OF_AVAILABLE_THREADS;
        }
    }

    /**
     * @throws Throwable
     * @throws InvalidConfigException
     * @throws StaleObjectException
     */
    public function run(): void
    {
        $waitTasksQuery = QueueTask::find()->whereStatusIs(QueueTask::STATUS_WAIT)->orderByPriority()->limit(1);
        $runningTasksQuery = QueueTask::find()->whereStatusIs(QueueTask::STATUS_RUN)->onlyId();

        /** @var QueueTask $task */
        while(($task = $waitTasksQuery->limit(1)->one())) {
            while($runningTasksQuery->count() == $this->maxThreadsAvailable) {
                sleep(self::SLEEP_TIME_IN_SECONDS_IF_THREADS_IS_BUSY);
                $this->excludeDeadJobs();
            }

            try {
                $command = sprintf(self::RUN_JOB_CONSOLE_COMMAND_TEMPLATE, $this->pathToYii, $task->id);
                ConsoleHelper::runAsyncAndGetPid($command);
                $task->setIsRunning();
            } catch (ConsoleException $exception) {
                $task->setError($exception, $this->attempts);
            }
        }
    }

    /**
     * @throws InvalidConfigException
     * @throws StaleObjectException
     * @throws Throwable
     */
    private function excludeDeadJobs(): void
    {
        /** @var QueueTask[] $runningTasks */
        $runningTasks = QueueTask::find()
            ->whereStatusIs(QueueTask::STATUS_RUN)
            ->whereRunTimeMoreThen($this->maxSecondsToCompleteTask)
            ->all();
        foreach($runningTasks as $runningTask) {
            $runningTask->setError(new LogicException(self::DEAD_JOB_EXCEPTION_MESSAGE), $this->attempts);
        }
    }

    /**
     * @throws InvalidConfigException
     */
    public function listen(): void
    {
        $tasksQuery = QueueTask::find()->whereStatusIs(QueueTask::STATUS_WAIT);
        while (true) {
            try {
                while(!$tasksQuery->exists()) {
                    sleep(self::SLEEP_TIME_IN_SECONDS_IF_QUEUE_IS_EMPTY);
                }

                $this->run();
            } catch (Throwable $exception) {
                echo Utils::getErrorMessageByException($exception) . PHP_EOL;
            }
        }
    }

    /**
     * @param int $taskId
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function runJob(int $taskId): void
    {
        $task = null;
        try {
            $task = QueueTask::getById($taskId);
            $job = $this->getJobByTask($task);

            $job->run();
            $task->setIsFinished();
        } catch (Throwable $exception) {
            if($task) {
                $task->setError($exception, $this->attempts);
            }
        }
    }

    /**
     * @param QueueTask $task
     * @return IJob
     * @throws LogicException
     */
    private function getJobByTask(QueueTask $task): IJob
    {
        try {
            $jobReflection = new ReflectionClass($task->class_name);
            if($jobReflection->implementsInterface(IJob::class)) {
                return $jobReflection->newInstance(json_decode($task->params, true));
            } else {
                throw new LogicException('Указанный для Job-а класс не реализует интерфейс IJob');
            }
        } catch (Throwable $exception) {
            throw new LogicException('Ошибка получения IJob: ' . $exception->getMessage());
        }
    }

    public function holdProcessWhileJobsIsRunning(): void
    {
        $tasksQuery = QueueTask::find()->whereStatusIs(QueueTask::STATUS_RUN);
        while($tasksQuery->exists()) {
            sleep(self::SLEEP_TIME_ON_HOLD_PROCESS);
        }
    }

    public function clear(): void
    {
        QueueTask::deleteAll(['status' => QueueTask::STATUS_WAIT]);
    }
}