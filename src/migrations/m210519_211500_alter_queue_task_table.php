<?php

use queue\models\QueueTask;
use yii\db\Migration;

class m210519_211500_alter_queue_task_table extends Migration
{
    const TABLE_NAME = 'queue_task';
    const COLUMN = 'priority';
    
    public function safeUp()
    {
        $this->addColumn(
            self::TABLE_NAME,
            self::COLUMN,
            $this->integer(3)->notNull()->defaultValue(QueueTask::PRIORITY_DEFAULT)->comment('Приоритет')
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            self::TABLE_NAME,
            self::COLUMN
        );
    }
}