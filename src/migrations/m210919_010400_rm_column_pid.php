<?php

use queue\models\QueueTask;
use yii\db\Migration;

class m210919_010400_rm_column_pid extends Migration
{
    const TABLE_NAME = 'queue_task';
    const COLUMN = 'pid';
    
    public function safeUp()
    {
        $this->dropColumn(
            self::TABLE_NAME,
            self::COLUMN
        );
    }

    public function safeDown()
    {
        $this->addColumn(
            self::TABLE_NAME,
            self::COLUMN,
            $this->integer(6)->comment('Id процесса')
        );
    }
}