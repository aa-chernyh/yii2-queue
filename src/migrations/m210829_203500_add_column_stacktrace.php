<?php

use queue\models\QueueTask;
use yii\db\Migration;

class m210829_203500_add_column_stacktrace extends Migration
{
    const TABLE_NAME = 'queue_task';
    const COLUMN = 'stacktrace';
    
    public function safeUp()
    {
        $this->addColumn(
            self::TABLE_NAME,
            self::COLUMN,
            $this->text()->comment('StackTrace')
        );
    }

    public function safeDown()
    {
        $this->dropColumn(
            self::TABLE_NAME,
            self::COLUMN
        );
    }
}