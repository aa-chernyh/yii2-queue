<?php

namespace queue\helpers;

use queue\exceptions\ConsoleException;
use queue\exceptions\LogicException;
use \Throwable;

class ConsoleHelper
{

    const COMMAND_GET_CPU_THREADS = 'cat /proc/cpuinfo | grep processor | wc -l';
    const ASYNC_COMMAND_ENDING_RETURNS_PID = ' > /dev/null 2>&1 & echo $!; ';
    const COMMAND_IS_PROCESS_RUNNING = 'ps -p %d | grep %d';

    /**
     * @return int
     * @throws ConsoleException
     */
    public static function getCountOfThreads(): int
    {
        return intval(self::runAndGetResult(self::COMMAND_GET_CPU_THREADS));
    }

    /**
     * @param string $command
     * @return string|null
     * @throws ConsoleException
     */
    private static function runAndGetResult(string $command): ?string
    {
        try {
            return shell_exec($command);
        } catch (Throwable $exception) {
            throw new ConsoleException('Ошибка выполнения запроса в консоль. Команда: ' . $command);
        }
    }

    /**
     * @param string $command
     * @return int
     * @throws ConsoleException
     */
    public static function runAsyncAndGetPid(string $command): int
    {
        try {
            $command = $command . self::ASYNC_COMMAND_ENDING_RETURNS_PID;
            $result = exec($command, $output);
            $pid = intval($result);
            if(!$pid) {
                throw new LogicException('');
            }
            return $pid;
        } catch (Throwable $exception) {
            throw new ConsoleException('Ошибка выполнения асинхронного запроса в консоль. Команда: ' . $command);
        }
    }
}