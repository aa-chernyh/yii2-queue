<?php

namespace queue\helpers;

use DateTime;
use Throwable;

class Utils
{

    const DATETIME_FORMAT = 'Y-m-d H:i:s';

    public static function getDbDateTimeStringByDateTime(DateTime $dateTime): string
    {
        return $dateTime->format(self::DATETIME_FORMAT);
    }

    public static function getErrorMessageByException(Throwable $exception): string
    {
        return 'File: ' . $exception->getFile()
            . '. Line: ' . $exception->getLine()
            . '. Message: ' . $exception->getMessage();
    }
}