# yii2-queue

Очередь, основанная на cli запуске php. Автоматически параллелит процессы до максимального количества потоков процессора.

### Подключение
- В файле общей конфигурации приложения (для Yii2 basic это /backend/config/common.php) добавить следующие строки:
```text
<?php
$config = [
    ...
    'bootstrap' => [
        'queue',
    ],
    'components' => [
        'queue' => [
            'class' => queue\Queue::class,
            'pathToYii' => '/path/to/project/yii',
            'attempts' => 3 //По умолчанию 1
            'maxSecondsToCompleteTask' => 60 //По умолчанию 10800 секунд (3 часа)
        ],
    ],
    ...
];
```

- Применить миграции, расположенные в папке /path/to/project/vendor/aa-chernyh/yii2-queue/src/migrations/

### Использование

- Создайте Job, который будет описывать выполняемую задачу. Пример можно посмотреть в файле 
  ```text
  /path/to/project/vendor/aa-chernyh/yii2-queue/src/models/JobExample.php
  ```
  Затем в любом месте проекта можно выполнить 
  ```text
  $priority = QueueTask::PRIORITY_DEFAULT; //от QueueTask::PRIORITY_MIN до QueueTask::PRIORITY_MAX
  \Yii::$app->queue->push(new JobExample([
      'param1' => 'value'
  ]), $priority);
  ```
  Таким образом задача поместится в очередь.

- Чтобы запустить выполнение задач, необходимо выполнить в консоли
  ```text
  php /path/to/project/yii queue/run (единоразовое выполнение всех тасков)
  ```
  или
  ```text
  php /path/to/project/yii queue/listen (постоянное слежение за появлением тасков)
  ```